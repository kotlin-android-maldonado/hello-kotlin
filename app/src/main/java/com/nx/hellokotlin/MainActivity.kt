package com.nx.hellokotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.widget.Button
import android.widget.TextView
import java.util.*

class MainActivity : AppCompatActivity(), TextToSpeech.OnInitListener {

    var tts: TextToSpeech? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tts = TextToSpeech(this, this)
        findViewById<Button>(R.id.btnPlay).setOnClickListener { speak() }
    }

    private fun speak() {
        var menssage: String = findViewById<TextView>(R.id.etMessage).text.toString()
        if (menssage.isEmpty()){
            findViewById<TextView>(R.id.tvStatus).text = "Introduzca un texto"
            menssage = "Creo que te falto introducir un texto"
        }
        tts?.speak(menssage, TextToSpeech.QUEUE_FLUSH, null, "")
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            findViewById<TextView>(R.id.tvStatus).text = "Listo!"
            tts?.language = Locale("ES")
        } else {
            findViewById<TextView>(R.id.tvStatus).text = "No Disponible"
        }
    }

    override fun onDestroy() {
        if (tts != null) {
            tts?.stop()
            tts?.shutdown()
        }
        super.onDestroy()
    }
}